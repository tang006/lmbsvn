package com.lmbsvn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * 启动类
 */
@SpringBootApplication
@ComponentScan(basePackages="com.lmbsvn.*")
@EnableJpaRepositories(basePackages="com.lmbsvn.*")
@EntityScan(basePackages="com.lmbsvn.*")
public class LmbsvnApplication {

    public static void main(String[] args) {
        SpringApplication.run(LmbsvnApplication.class, args);
        System.out.println("启动完成。。。。。。。。。。。");
    }
}
