package com.lmbsvn.core.controller;

import com.lmbsvn.core.bean.ExceptionInfo;
import com.lmbsvn.core.common.BusinessException;
import com.lmbsvn.core.shiro.bean.CaptchaException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *  Controller基类
 */
public class BaseController {

	/** 基于@ExceptionHandler异常处理 */
	@ExceptionHandler(Exception.class)
	public @ResponseBody
    Object resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
		ExceptionInfo exinfo = new ExceptionInfo();
		if(ex instanceof BusinessException) {
			exinfo.setIsException(true);
			exinfo.setCode(999);
			exinfo.setType("bizexception");
			exinfo.setTitle("异常");
			exinfo.setMsg(ex.getMessage());
			exinfo.setDetails(ex.toString());
		}else if(ex instanceof IncorrectCredentialsException){
			exinfo.setIsException(true);
			exinfo.setCode(999);
			exinfo.setType("bizexception");
			exinfo.setTitle("提示");
			exinfo.setDetails(ex.toString());
			exinfo.setMsg("密码错误！");
		}else if(ex instanceof UnknownAccountException){
			exinfo.setIsException(true);
			exinfo.setCode(999);
			exinfo.setType("bizexception");
			exinfo.setTitle("提示");
			exinfo.setDetails(ex.toString());
			exinfo.setMsg("该用户不存在！");
		}else if(ex instanceof CaptchaException){
			exinfo.setIsException(true);
			exinfo.setCode(999);
			exinfo.setType("bizexception");
			exinfo.setTitle("提示");
			exinfo.setDetails(ex.toString());
			exinfo.setMsg("验证码错误！");
		}else if(ex instanceof ExcessiveAttemptsException){
			exinfo.setIsException(true);
			exinfo.setCode(999);
			exinfo.setType("bizexception");
			exinfo.setTitle("提示");
			exinfo.setDetails(ex.toString());
			exinfo.setMsg("密码多次错误，请10分钟后重试！");
		}else if(ex instanceof LockedAccountException){
			exinfo.setIsException(true);
			exinfo.setCode(999);
			exinfo.setType("bizexception");
			exinfo.setTitle("提示");
			exinfo.setDetails(ex.toString());
			exinfo.setMsg("账号已被锁定,请联系管理员!");
		}else {
			exinfo.setIsException(true);
			exinfo.setCode(999);
			exinfo.setType("exception");
			exinfo.setTitle("异常");
			exinfo.setDetails(ex.toString());
			exinfo.setMsg(ex.toString());
			ex.printStackTrace();
		}
		return exinfo;
	}
}
