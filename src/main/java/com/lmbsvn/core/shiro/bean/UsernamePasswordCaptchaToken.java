package com.lmbsvn.core.shiro.bean;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * 扩展默认的用户认证的bean
 * @author Administrator
 */
public class UsernamePasswordCaptchaToken extends UsernamePasswordToken{
	
	private static final long serialVersionUID = 1L;
	 
	private String captcha; //验证码
	private String strpassword; //字符串类型的密码
 
	public String getCaptcha() {
		return captcha;
	}
 
	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}
	
	public String getStrpassword() {
		return strpassword;
	}

	public void setStrpassword(String strpassword) {
		this.strpassword = strpassword;
	}

	public UsernamePasswordCaptchaToken() {
		super();
	}
	
	public UsernamePasswordCaptchaToken(String username, String password, String captcha){
		super(username, password);
		this.strpassword = password;
		this.captcha = captcha;
	}
	
	public UsernamePasswordCaptchaToken(String username, String password, boolean rememberMe, String host, String captcha) {
		super(username, password, rememberMe, host);
		this.captcha = captcha;
	}
}
