package com.lmbsvn.core.util;

import com.lmbsvn.core.bean.Result;

/**
 * @Description 返回对象工具类
 * @Author 芦明宝
 * @Date 2019-08-12 9:06
 * @Version 1.0
 */
public class ResultUtil {

    /**
     *  返回成功，有数据信息
     * @param object
     * @return
     */
    public static Result success(Object object) {
        Result result = new Result();
        result.setCode(0);
        result.setMsg("成功");
        result.setData(object);
        return result;
    }

    /**
     * 返回成功，没有数据信息
     * @return
     */
    public static Result success() {
        return success(null);
    }

    /**
     * 返回错误
     * @param code
     * @param msg
     * @param object
     * @return
     */
    public static Result error(Integer code, String msg, Object object) {
        Result result = new Result();
        result.setCode(code);
        result.setMsg(msg);
        result.setData(object);
        return result;
    }
}
