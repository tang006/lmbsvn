package com.lmbsvn.core.util;

import org.wltea.analyzer.IKSegmentation;
import org.wltea.analyzer.Lexeme;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import java.util.Map.Entry;

public class Util {
	
	/**
	 * 将map的所有key转大写
	 * @param map
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map keyToUpperCase(Map map){
		Map newmap = new HashMap();
		Iterator it = map.entrySet().iterator();
		while(it.hasNext()){
			Entry entry = (Entry)it.next();
			newmap.put(entry.getKey().toString().toUpperCase(), entry.getValue());
		}
		return newmap;
	}
	
	/**
	 * 将map的所有key转小写
	 * @param map
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map keyToLowerCase(Map map){
		Map newmap = new HashMap();
		Iterator it = map.entrySet().iterator();
		while(it.hasNext()){
			Entry entry = (Entry)it.next();
			newmap.put(entry.getKey().toString().toLowerCase(), entry.getValue());
		}
		return newmap;
	}
	
	/**
	 * 分词
	 * @param text
	 * @return
	 * @throws IOException
	 */
	public static List<String> Fengci(String text) throws IOException{
		List<String> list = new ArrayList<String>();
		StringReader sr = new StringReader(text);
		IKSegmentation ik = new IKSegmentation(sr, true);
		Lexeme lex = null;
		while((lex = ik.next()) != null){
			list.add(lex.getLexemeText());
		}
		return list;
	}
}
