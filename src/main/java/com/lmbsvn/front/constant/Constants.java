package com.lmbsvn.front.constant;

public class Constants {
    /**
     * svn协议
     */
    public static final String SVN = "svn";
    /**
     * http单库
     */
    public static final String HTTP = "http";
    /**
     * http多库
     */
    public static final String HTTP_MUTIL = "http-mutil";
}
