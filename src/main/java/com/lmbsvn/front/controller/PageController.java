package com.lmbsvn.front.controller;

import com.lmbsvn.core.bean.FsdModelAndView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/")
public class PageController {

	/**
	 * 登录
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login.htm")
	public FsdModelAndView toLoginPage(HttpServletRequest request, HttpServletResponse response) throws Exception{
		FsdModelAndView mav = new FsdModelAndView(request);
		mav.setViewName("login");
		return mav;
	}

	/**
	 * 首页
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/index.htm")
	public void toIndexPage(HttpServletRequest request, HttpServletResponse response) throws Exception{
		response.sendRedirect("projectManage.htm");
	}

	/**
	 * 项目管理
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/projectManage.htm")
	public ModelAndView toProjectManagePage(HttpServletRequest request, HttpServletResponse response) throws Exception{
		FsdModelAndView mav = new FsdModelAndView(request);
		mav.addObject("type","1");
		mav.setViewName("projectManage");
		return mav;
	}

	/**
	 * 添加项目
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/projectAdd.htm")
	public FsdModelAndView toAddProjectPage(HttpServletRequest request, HttpServletResponse response) throws Exception{
		FsdModelAndView mav = new FsdModelAndView(request);
		mav.addObject("type","1");
		mav.setViewName("projectAdd");
		return mav;
	}

	/***
	 * 用户管理
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/userManage.htm")
	public FsdModelAndView toUserManagePage(HttpServletRequest request, HttpServletResponse response) throws Exception{
		FsdModelAndView mav = new FsdModelAndView(request);
		mav.addObject("type","2");
		mav.setViewName("userManage");
		return mav;
	}

	/***
	 * 添加用户
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/userAdd.htm")
	public FsdModelAndView toUserAddPage(HttpServletRequest request, HttpServletResponse response) throws Exception{
		FsdModelAndView mav = new FsdModelAndView(request);
		mav.addObject("type","2");
		mav.setViewName("userAdd");
		return mav;
	}

	/**
	 * 修改密码
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/userPassword.htm")
	public FsdModelAndView toUserPasswordPage(HttpServletRequest request, HttpServletResponse response) throws Exception{
		FsdModelAndView mav = new FsdModelAndView(request);
		mav.addObject("type","3");
		mav.setViewName("userPassword");
		return mav;
	}
}
