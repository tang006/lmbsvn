package com.lmbsvn.front.controller;

import com.lmbsvn.core.controller.BaseController;
import com.lmbsvn.core.util.ResultUtil;
import com.lmbsvn.front.model.Hd_svnproject;
import com.lmbsvn.front.model.Hd_svnuser;
import com.lmbsvn.front.service.Hd_svnprojectService;
import com.lmbsvn.front.service.Hd_svnuserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/data")
public class ajaxController extends BaseController {

    @Autowired
    private Hd_svnprojectService projectService;

    @Autowired
    private Hd_svnuserService userService;

    /**
     * 加载用户分页JSON数据
     * @param page 页码
     * @param size 每页条数
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/user_loaddata")
    public @ResponseBody Object getUserPageList(int page, int size, HttpServletRequest request) throws Exception {
        Page<Hd_svnuser> users = userService.getUserPageList(page, size);
        return ResultUtil.success(users);
    }

    /**
     * 编辑用户
     * @param user
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/user_editor")
    public @ResponseBody
    Object saveUser(Hd_svnuser user, HttpServletRequest request, HttpServletResponse response) throws Exception {
        userService.editorUser(user);
        return ResultUtil.success();
    }

    /**
     * 加载项目分页JSON数据
     * @param page
     * @param size
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/project_loaddata")
    public @ResponseBody
    Object getProjectPageList(int page, int size, HttpServletRequest request) throws Exception {
        Page<Hd_svnproject> subjects = projectService.getProjectPageList(page, size);
        return ResultUtil.success(subjects);
    }

    /**
     * 编辑项目
     * @param project
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/project_editor")
    public @ResponseBody
    Object saveProject(Hd_svnproject project, HttpServletRequest request, HttpServletResponse response) throws Exception {
        projectService.addProject(project);
        return ResultUtil.success();
    }
}
