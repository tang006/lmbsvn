package com.lmbsvn.front.freemarker;

import com.lmbsvn.front.freemarker.tag.DemoTag;
import com.lmbsvn.front.freemarker.tag.UserTag;
import freemarker.template.Configuration;
import freemarker.template.TemplateModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

@Component
public class FreemarkerConfig {

    @Autowired
    private Configuration configuration;

    @Autowired
    DemoTag demoTag;

    @Autowired
    UserTag userTag;


    @PostConstruct
    public void setSharedVariable() throws TemplateModelException {
        configuration.setSharedVariable("demoTag", demoTag);
        configuration.setSharedVariable("userTag", userTag);
    }
}
