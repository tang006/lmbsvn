package com.lmbsvn.front.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="hd_svnauth")
public class Hd_svnauth{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_projectid;
	private String f_groupid;
	private String f_resources;
	private String f_auth;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_projectid() {
		return f_projectid;
	}
	public void setF_projectid(String f_projectid) {
		this.f_projectid = f_projectid;
	}
    
	public String getF_groupid() {
		return f_groupid;
	}
	public void setF_groupid(String f_groupid) {
		this.f_groupid = f_groupid;
	}
    
	public String getF_resources() {
		return f_resources;
	}
	public void setF_resources(String f_resources) {
		this.f_resources = f_resources;
	}
    
	public String getF_auth() {
		return f_auth;
	}
	public void setF_auth(String f_auth) {
		this.f_auth = f_auth;
	}
    
}
