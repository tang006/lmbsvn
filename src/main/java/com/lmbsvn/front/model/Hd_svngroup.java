package com.lmbsvn.front.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="hd_svngroup")
public class Hd_svngroup{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_projectid;
	private String f_group;
	private String f_describe;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_projectid() {
		return f_projectid;
	}
	public void setF_projectid(String f_projectid) {
		this.f_projectid = f_projectid;
	}
    
	public String getF_group() {
		return f_group;
	}
	public void setF_group(String f_group) {
		this.f_group = f_group;
	}
    
	public String getF_describe() {
		return f_describe;
	}
	public void setF_describe(String f_describe) {
		this.f_describe = f_describe;
	}
    
}
