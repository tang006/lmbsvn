package com.lmbsvn.front.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="hd_svnproject")
public class Hd_svnproject{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_name;
	private String f_type;
	private String f_path;
	private String f_url;
	private String f_describe;
	private String f_adddate;
	private String f_adduser;
	private String f_delete;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
    
	public String getF_type() {
		return f_type;
	}
	public void setF_type(String f_type) {
		this.f_type = f_type;
	}
    
	public String getF_path() {
		return f_path;
	}
	public void setF_path(String f_path) {
		this.f_path = f_path;
	}
    
	public String getF_url() {
		return f_url;
	}
	public void setF_url(String f_url) {
		this.f_url = f_url;
	}
    
	public String getF_describe() {
		return f_describe;
	}
	public void setF_describe(String f_describe) {
		this.f_describe = f_describe;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_adduser() {
		return f_adduser;
	}
	public void setF_adduser(String f_adduser) {
		this.f_adduser = f_adduser;
	}
    
	public String getF_delete() {
		return f_delete;
	}
	public void setF_delete(String f_delete) {
		this.f_delete = f_delete;
	}
    
}
