package com.lmbsvn.front.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="hd_svnprojectusergroup")
public class Hd_svnprojectusergroup{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_projectid;
	private String f_groupid;
	private String f_userid;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_projectid() {
		return f_projectid;
	}
	public void setF_projectid(String f_projectid) {
		this.f_projectid = f_projectid;
	}
    
	public String getF_groupid() {
		return f_groupid;
	}
	public void setF_groupid(String f_groupid) {
		this.f_groupid = f_groupid;
	}
    
	public String getF_userid() {
		return f_userid;
	}
	public void setF_userid(String f_userid) {
		this.f_userid = f_userid;
	}
    
}
