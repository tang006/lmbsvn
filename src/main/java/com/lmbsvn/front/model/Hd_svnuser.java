package com.lmbsvn.front.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="hd_svnuser")
public class Hd_svnuser{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_username;
	private String f_realname;
	private String f_password;
	private String f_role;
	private String f_adddate;
	private String f_adduser;
	private String f_delete;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_username() {
		return f_username;
	}
	public void setF_username(String f_username) {
		this.f_username = f_username;
	}
    
	public String getF_realname() {
		return f_realname;
	}
	public void setF_realname(String f_realname) {
		this.f_realname = f_realname;
	}
    
	public String getF_password() {
		return f_password;
	}
	public void setF_password(String f_password) {
		this.f_password = f_password;
	}
    
	public String getF_role() {
		return f_role;
	}
	public void setF_role(String f_role) {
		this.f_role = f_role;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_adduser() {
		return f_adduser;
	}
	public void setF_adduser(String f_adduser) {
		this.f_adduser = f_adduser;
	}
    
	public String getF_delete() {
		return f_delete;
	}
	public void setF_delete(String f_delete) {
		this.f_delete = f_delete;
	}
    
}
