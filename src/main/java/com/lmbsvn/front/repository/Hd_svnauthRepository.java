package com.lmbsvn.front.repository;

import com.lmbsvn.front.model.Hd_svnauth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;
import java.util.Map;

public interface Hd_svnauthRepository extends JpaRepository<Hd_svnauth, String> {
    /**
     * 获得当前项目所有被授权的资源
     * @param projectid
     * @return
     */
    @Query(value = "select svnauth.f_resources from Hd_svnauth svnauth where svnauth.f_projectid = :projectid group by svnauth.f_resources", nativeQuery = true)
    List<Map<String, String>> getResourcesByProjectId(@Param("projectid") String projectid);

    /**
     * 根据项目ID，分组ID，资源加载授权情况
     * @param projectid
     * @param groupid
     * @param resources
     * @return
     */
    @Query("select svnauth from Hd_svnauth svnauth where svnauth.f_projectid = :projectid and svnauth.f_groupid = :groupid and svnauth.f_resources = :resources")
    List<Hd_svnauth> getSvnauthByProjectIdAndGroupidAndResources(@Param("projectid") String projectid, @Param("groupid") String groupid, @Param("resources") String resources);
}

