package com.lmbsvn.front.repository;

import com.lmbsvn.front.model.Hd_svngroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface Hd_svngroupRepository extends JpaRepository<Hd_svngroup, String> {

    /**
     * 获得项目所有的用户组
     * @param projectid
     * @return
     */
    @Query("select svngroup from Hd_svngroup svngroup where svngroup.f_projectid = :projectid")
    List<Hd_svngroup> getListByProjectId(@Param("projectid") String projectid);
}

