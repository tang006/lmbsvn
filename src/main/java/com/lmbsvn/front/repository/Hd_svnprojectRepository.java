package com.lmbsvn.front.repository;

import com.lmbsvn.front.model.Hd_svnproject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface Hd_svnprojectRepository extends JpaRepository<Hd_svnproject, String> {

    /**
     * 根据项目名称获得项目
     * @param f_name
     * @return
     */
    @Query("select project from Hd_svnproject project where project.f_name = :f_name")
    List<Hd_svnproject> getProjectByName(@Param("f_name") String f_name);

    /**
     * 根据项目路径加载项目
     * @param f_path
     * @return
     */
    @Query("select project from Hd_svnproject project where project.f_path = :f_path")
    List<Hd_svnproject> getProjectByPath(@Param("f_path") String f_path);

    /**
     * 根据项目URL加载项目
     * @param f_url
     * @return
     */
    @Query("select project from Hd_svnproject project where project.f_url = :f_url")
    List<Hd_svnproject> getProjectByUrl(@Param("f_url") String f_url);
}

