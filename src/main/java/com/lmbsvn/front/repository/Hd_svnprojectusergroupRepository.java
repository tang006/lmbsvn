package com.lmbsvn.front.repository;

import com.lmbsvn.front.model.Hd_svnprojectusergroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface Hd_svnprojectusergroupRepository extends JpaRepository<Hd_svnprojectusergroup, String> {

    /**
     * 根据项目ID获得列表
     * @param projectid
     * @return
     */
    @Query("select svnprojectusergroup from Hd_svnprojectusergroup svnprojectusergroup where svnprojectusergroup.f_projectid = :projectid")
    List<Hd_svnprojectusergroup> getListByProjectId(@Param("projectid") String projectid);

    /**
     * 根据项目ID和分组ID获得列表
     * @param projectid
     * @param groupid
     * @return
     */
    @Query("select svnprojectusergroup from Hd_svnprojectusergroup svnprojectusergroup where svnprojectusergroup.f_projectid = :projectid and svnprojectusergroup.f_groupid = :groupid")
    List<Hd_svnprojectusergroup> getListByProjectIdAndGroupId(@Param("projectid") String projectid, @Param("groupid") String groupid);
}

