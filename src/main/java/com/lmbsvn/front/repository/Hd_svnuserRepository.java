package com.lmbsvn.front.repository;

import com.lmbsvn.front.model.Hd_svnuser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface Hd_svnuserRepository extends JpaRepository<Hd_svnuser, String> {

    /**
     * 根据用户名加载列表
     * @param username
     * @return
     */
    @Query("select svnuser from Hd_svnuser svnuser where svnuser.f_username = :username")
    List<Hd_svnuser> getListByUsername(@Param("username") String username);

    /**
     * 根据一组用户ID加载列表
     * @param ids
     * @return
     */
    @Query("select svnuser from Hd_svnuser svnuser where id in(:ids)")
    List<Hd_svnuser> getListByUserids(@Param("ids") String ids);
}
