package com.lmbsvn.front.service;

import org.tmatesoft.svn.core.SVNURL;
import java.io.File;

public interface Hd_repositoryService {
    /**
     * 创建本地仓库
     * @param respository
     * @return
     * @throws Exception
     */
    SVNURL createLocalRepository(File respository) throws Exception;
    /**
     * 从项目的url中获取svn的url
     * @param url 项目URL
     * @return SVN URL
     */
    String parseURL(String url);
}
