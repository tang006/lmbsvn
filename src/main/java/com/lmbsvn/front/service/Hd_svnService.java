package com.lmbsvn.front.service;

import com.lmbsvn.front.model.Hd_svnproject;

public interface Hd_svnService {
    /**
     * 导出到配置文件
     * @param id
     * @throws Exception
     */
    void exportConfig(String id) throws Exception;
    /**
     * 导出到配置文件
     * @param project
     * @throws Exception
     */
    void exportConfig(Hd_svnproject project) throws Exception;
}
