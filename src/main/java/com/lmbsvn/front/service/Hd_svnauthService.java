package com.lmbsvn.front.service;

import com.lmbsvn.front.model.Hd_svnauth;
import com.lmbsvn.front.model.Hd_svnproject;
import java.util.List;
import java.util.Map;

public interface Hd_svnauthService {
    /**
     * 格式化资源.如果资源没有[],自动加上[relateRoot:/]
     * @param projectId
     * @param resources
     * @return
     * @throws Exception
     */
    String formatResources(String projectId, String resources) throws Exception;
    /**
     * 格式化资源.如果资源没有[],自动加上[relateRoot:/]
     * @param project
     * @param resources
     * @return
     * @throws Exception
     */
    String formatResources(Hd_svnproject project, String resources) throws Exception;
    /**
     * 根据项目ID，加载该项目下每一个资源的授权情况
     * @param projectId
     * @return
     * @throws Exception
     */
    Map<String ,List<Hd_svnauth>> getMapByProjectId(String projectId) throws Exception;
}
