package com.lmbsvn.front.service;

import com.lmbsvn.front.model.Hd_svngroup;
import java.util.List;

public interface Hd_svngroupService {
    /**
     * 根据项目ID加载项目用户组列表
     * @param projectId
     * @return
     * @throws Exception
     */
    List<Hd_svngroup> getListByProjectId(String projectId) throws Exception;

    /**
     * 根据ID加载分组信息
     * @param id
     * @return
     * @throws Exception
     */
    Hd_svngroup getSvngroupById(String id) throws Exception;
}
