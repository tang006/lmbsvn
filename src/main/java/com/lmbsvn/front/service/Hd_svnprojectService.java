package com.lmbsvn.front.service;

import com.lmbsvn.front.model.Hd_svnproject;
import org.springframework.data.domain.Page;

public interface Hd_svnprojectService {
    /**
     * 加载项目分页列表数据
     * @param page
     * @param size
     * @return
     * @throws Exception
     */
    Page<Hd_svnproject> getProjectPageList(int page, int size) throws Exception;
    /**
     * 新增项目
     * @param project
     * @throws Exception
     */
    void addProject(Hd_svnproject project) throws Exception;
    /**
     * 获取项目的相对根路径.例如项目的path=e:/svn/projar，则返回projar。如果path为空，则返回项目ID
     * @param projectId
     * @return
     * @throws Exception
     */
    String getRelateRootPath(String projectId) throws Exception;
    /**
     * 获取项目的相对根路径.例如项目的path=e:/svn/projar，则返回projar。如果path为空，则返回项目ID
     * @param project
     * @return
     * @throws Exception
     */
    String getRelateRootPath(Hd_svnproject project) throws Exception;
    /**
     * 根据项目ID加载项目
     * @param projectId
     * @return
     * @throws Exception
     */
    Hd_svnproject getSvnproject(String projectId) throws Exception;
}
