package com.lmbsvn.front.service;

import com.lmbsvn.front.model.Hd_svnprojectusergroup;
import java.util.List;
import java.util.Map;

public interface Hd_svnprojectusergroupService {
    /**
     * 根据项目ID获得列表
     * @param projectid
     * @return
     */
    List<Hd_svnprojectusergroup> getListByProjectId(String projectid) throws Exception;
    /**
     * 更具项目ID的获得该项目对应的用户组下的用户列表
     * @param projectId
     * @return
     * @throws Exception
     */
    Map<String, List<Hd_svnprojectusergroup>> getMapByProjectId(String projectId) throws Exception;
}
