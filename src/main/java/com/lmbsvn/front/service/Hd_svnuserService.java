package com.lmbsvn.front.service;

import com.lmbsvn.front.model.Hd_svnuser;
import org.springframework.data.domain.Page;
import java.util.List;

public interface Hd_svnuserService {
    /**
     * 加载用户分页列表数据
     * @param page
     * @param size
     * @return
     * @throws Exception
     */
    Page<Hd_svnuser> getUserPageList(int page, int size) throws Exception;
    /**
     * 根据用户名加载对象
     * @param username
     * @return
     * @throws Exception
     */
    Hd_svnuser getObjectByUsername(String username) throws Exception;
    /**
     * 编辑用户
     * @param user
     * @throws Exception
     */
    void editorUser(Hd_svnuser user) throws Exception;
    /**
     * 根据项目ID获得用户列表
     * @param projectid
     * @return
     * @throws Exception
     */
    List<Hd_svnuser> getListByProjectId(String projectid) throws  Exception;

    /**
     * 根据用户ID加载用户信息
     * @param id
     * @return
     * @throws Exception
     */
    Hd_svnuser getSvnuserById(String id) throws Exception;
}
