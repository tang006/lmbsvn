package com.lmbsvn.front.service.impl;

import com.lmbsvn.front.service.Hd_repositoryService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import java.io.File;

@Service("hd_repositoryserviceimpl")
public class Hd_repositoryServiceImpl implements Hd_repositoryService {

    /**
     * 创建本地仓库
     * @param respository
     * @return
     * @throws Exception
     */
    public SVNURL createLocalRepository(File respository) throws Exception {
        return SVNRepositoryFactory.createLocalRepository(respository, true, false);
    }

    /**
     * 从项目的url中获取svn的url
     * @param url 项目URL
     * @return SVN URL
     */
    public String parseURL(String url){
        if(StringUtils.isBlank(url)){
            return null;
        }
        String result = url.trim();//去空格
        result = StringUtils.replace(result, "\t", " ");
        result = StringUtils.replace(result, "\r", " ");
        result = StringUtils.replace(result, "\n", " ");
        result = StringUtils.replace(result, "\b", " ");
        result = StringUtils.replace(result, "<", " ");//eg. <br/>
        result = StringUtils.replace(result, "(", " ");//eg. ()

        result = result.trim();
        int blank = result.indexOf(" ");
        if(blank != -1){
            result = result.substring(0, blank);
        }
        return result;
    }
}
