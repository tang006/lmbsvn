package com.lmbsvn.front.service.impl;

import com.lmbsvn.front.repository.Hd_svngroupRepository;
import com.lmbsvn.front.model.Hd_svngroup;
import com.lmbsvn.front.service.Hd_svngroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service("hd_svngroupServiceImpl")
public class Hd_svngroupServiceImpl implements Hd_svngroupService {

	@Autowired
	private Hd_svngroupRepository svngroupRepository;

	/**
	 * 根据项目ID加载项目用户组列表
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Hd_svngroup> getListByProjectId(String projectId) throws Exception {
		return svngroupRepository.getListByProjectId(projectId);
	}


	@Override
	public Hd_svngroup getSvngroupById(String id) throws Exception {
		return svngroupRepository.getOne(id);
	}
}
