package com.lmbsvn.front.service.impl;

import com.lmbsvn.core.common.BusinessException;
import com.lmbsvn.front.repository.Hd_svnprojectRepository;
import com.lmbsvn.front.model.Hd_svnproject;
import com.lmbsvn.front.service.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service("hd_svnprojectServiceImpl")
public class Hd_svnprojectServiceImpl implements Hd_svnprojectService{

	@Autowired
	private Hd_svnprojectRepository svnprojectRepository;

	@Autowired
	private Hd_repositoryService repositoryService;

	@Autowired
	private Hd_svngroupService groupService;

	@Autowired
	private Hd_svnauthService authService;

	@Autowired
	private Hd_svnService svnService;

	/**
	 * 加载项目分页列表数据
	 * @param page
	 * @param size
	 * @return
	 * @throws Exception
	 */
	public Page<Hd_svnproject> getProjectPageList(int page, int size) throws Exception {
		PageRequest pageable = PageRequest.of(page-1, size);
		return svnprojectRepository.findAll(pageable);
	}

	/**
	 * 新增项目
	 * @param project
	 * @throws Exception
	 */
	public void addProject(Hd_svnproject project) throws Exception {
		// 路径 把\替换为/
		if (StringUtils.isNotBlank(project.getF_path())) {
			project.setF_path(StringUtils.replace(project.getF_path(), "\\", "/"));
		}

		// url 把\替换为/
		if (StringUtils.isNotBlank(project.getF_url())) {
			project.setF_url(StringUtils.replace(project.getF_url(), "\\", "/"));
		}

		if (checkName(project.getF_name())){
			throw new BusinessException("已存在该名称的项目，请修改后重试");
		}

		if (checkPath(project.getF_path())){
			throw new BusinessException("已存在该路径的项目，请修改后重试");
		}

		if (checkUrl(project.getF_url())){
			throw new BusinessException("已存在该URL的项目，请修改后重试");
		}

		// 创建仓库
		File respository = new File(project.getF_path());
		if (!respository.exists() || !respository.isDirectory()) {// 不存在仓库
			repositoryService.createLocalRepository(respository);
		}

//		// 增加默认组
//		for (String str : Constants.GROUPS) {
//			Hd_svngroup group = new Hd_svngroup();
//			group.setId(this.getUUID());
//			group.setF_projectid(project.getId());
//			group.setF_group(str);
//			group.setF_describe(str);
//			groupService.save(group);
//		}

//		// 增加默认权限
//		Hd_svnauth svnauth = new Hd_svnauth();
//		svnauth.setF_projectid(project.getId());
//		svnauth.setF_resources(authService.formatResources(project, "/"));
//		svnauth.setF_auth("rw");//r:读 w:写 rw:读写
//		svnauth.setF_group(Constants.GROUP_MANAGER);

		project.setId(UUID.randomUUID().toString().trim().replaceAll("-", ""));
		project.setF_adddate(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		project.setF_delete("0");
		project = svnprojectRepository.save(project);
		svnService.exportConfig(project.getId());
	}

	/**
	 * 检查项目名称是否重复
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public boolean checkName(String name) throws Exception{
		List<Hd_svnproject> list = svnprojectRepository.getProjectByName(name);
		if(list != null && list.size() > 0){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 检查项目路径是否重复
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public boolean checkPath(String path) throws Exception{
		List<Hd_svnproject> list = svnprojectRepository.getProjectByPath(path);
		if(list != null && list.size() > 0){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 检查项目URL是否重复
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public boolean checkUrl(String url) throws Exception{
		List<Hd_svnproject> list = svnprojectRepository.getProjectByUrl(url);
		if(list != null && list.size() > 0){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 获取项目的相对根路径.例如项目的path=e:/svn/projar，则返回projar。如果path为空，则返回项目ID
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public String getRelateRootPath(String projectId) throws Exception {
		Hd_svnproject project = svnprojectRepository.getOne(projectId);
		if(project == null || StringUtils.isBlank(project.getF_path())){
			return projectId;
		}
		return getRelateRootPath(project);
	}

	/**
	 * 获取项目的相对根路径.例如项目的path=e:/svn/projar，则返回projar。如果path为空，则返回项目ID
	 * @param project
	 * @return
	 * @throws Exception
	 */
	public String getRelateRootPath(Hd_svnproject project) throws Exception {
		String path = project.getF_path();
		if(StringUtils.isBlank(path)){
			return project.getId();
		}
		path = StringUtils.replace(path, "\\", "/");
		while(path.endsWith("/")){
			path = path.substring(0, path.length()-1);
		}
		return StringUtils.substringAfterLast(path, "/");
	}

	/**
	 * 根据项目ID加载项目
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	@Override
	public Hd_svnproject getSvnproject(String projectId) throws Exception {
		return svnprojectRepository.getOne(projectId);
	}
}
