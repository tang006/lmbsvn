package com.lmbsvn.front.service.impl;

import com.lmbsvn.front.repository.Hd_svnprojectusergroupRepository;
import com.lmbsvn.front.model.Hd_svngroup;
import com.lmbsvn.front.model.Hd_svnprojectusergroup;
import com.lmbsvn.front.service.Hd_svngroupService;
import com.lmbsvn.front.service.Hd_svnprojectusergroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("hd_svnprojectusergroupServiceImpl")
public class Hd_svnprojectusergroupServiceImpl implements Hd_svnprojectusergroupService{

	@Autowired
	private Hd_svnprojectusergroupRepository svnprojectusergroupRepository;

	@Autowired
	private Hd_svngroupService groupService;

	/**
	 * 根据项目ID获得列表
	 * @param projectId
	 * @return
	 */
	public List<Hd_svnprojectusergroup> getListByProjectId(String projectId) throws Exception{
		return svnprojectusergroupRepository.getListByProjectId(projectId);
	}

	/**
	 * 更具项目ID的获得该项目对应的用户组下的用户列表
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public Map<String, List<Hd_svnprojectusergroup>> getMapByProjectId(String projectId) throws Exception {
		Map<String, List<Hd_svnprojectusergroup>> map = new HashMap<String, List<Hd_svnprojectusergroup>>();
		List<Hd_svngroup> groupList = groupService.getListByProjectId(projectId);
		for(Hd_svngroup group : groupList){
			List<Hd_svnprojectusergroup> list = svnprojectusergroupRepository.getListByProjectIdAndGroupId(projectId, group.getId());
			map.put(group.getF_group(), list);
		}
		return map;
	}
}
