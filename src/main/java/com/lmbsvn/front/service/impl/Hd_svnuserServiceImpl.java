package com.lmbsvn.front.service.impl;

import com.lmbsvn.core.util.EncryptUtils;
import com.lmbsvn.front.repository.Hd_svnuserRepository;
import com.lmbsvn.front.model.Hd_svnprojectusergroup;
import com.lmbsvn.front.model.Hd_svnuser;
import com.lmbsvn.front.service.Hd_svnprojectusergroupService;
import com.lmbsvn.front.service.Hd_svnuserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service("hd_svnuserServiceImpl")
public class Hd_svnuserServiceImpl implements Hd_svnuserService{

	@Autowired
	private Hd_svnuserRepository svnuserRepository;

	@Autowired
	private Hd_svnprojectusergroupService projectusergroupService;

	/**
	 * 加载用户分页列表数据
	 * @param page
	 * @param size
	 * @return
	 * @throws Exception
	 */
	public Page<Hd_svnuser> getUserPageList(int page, int size) throws Exception {
		PageRequest pageable = PageRequest.of(page-1, size);
		return svnuserRepository.findAll(pageable);
	}

	/**
	 * 根据用户名加载对象
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public Hd_svnuser getObjectByUsername(String username) throws Exception{
		List<Hd_svnuser> list = svnuserRepository.getListByUsername(username);
		if (list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 编辑用户
	 * @param user
	 * @throws Exception
	 */
	public void editorUser(Hd_svnuser user) throws Exception {
		if(user.getId() != null && !"".equals(user.getId())){
			//修改
			Hd_svnuser old_user = svnuserRepository.getOne(user.getId());
			old_user.setF_username(user.getF_username());
			old_user.setF_realname(user.getF_realname());
			old_user.setF_password(EncryptUtils.md5Password(user.getF_password(), user.getF_password()));
			old_user.setF_role(user.getF_role());
			svnuserRepository.saveAndFlush(old_user);
		}else{
			//新增
			user.setId(UUID.randomUUID().toString().trim().replaceAll("-", ""));
			user.setF_password(EncryptUtils.md5Password(user.getF_password(), user.getF_password()));
			user.setF_adddate(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
			user.setF_delete("0");
			svnuserRepository.save(user);
		}
	}

	/**
	 * 根据项目ID获得拥有权限的用户列表
	 * @param projectid
	 * @return
	 * @throws Exception
	 */
	public List<Hd_svnuser> getListByProjectId(String projectid) throws  Exception {
		List<Hd_svnprojectusergroup> list = projectusergroupService.getListByProjectId(projectid);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			if(i < list.size()-1){
				sb.append(list.get(i).getF_userid() + ",");
			}else{
				sb.append(list.get(i).getF_userid());
			}
		}
		return svnuserRepository.getListByUserids(sb.toString());
	}

	/**
	 * 根据用户ID加载用户信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@Override
	public Hd_svnuser getSvnuserById(String id) throws Exception {
		return svnuserRepository.getOne(id);
	}
}
